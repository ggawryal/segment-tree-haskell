{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FunctionalDependencies #-}

module SegSegTree where 
    
import qualified Data.Semigroup 
import SegTreeBasis
import Monoids


--t = makeTree (0,10) :: (IndexedTree (BaseSegTreeData (Assignment Int) (Sum Int)) ClosedClosed)

fst' (a,_,_) = a
snd' (_,a,_) = a
trd' (_,_,a) = a

class SegTreeData a where 
    emptyData :: a
    merge :: (Indexing i) => a -> (i,i) -> a -> (i,i) -> a
    push :: (Indexing i) => a -> (i,i) -> (a, a->(i,i)->a, a->(i,i)->a)

__applyPush :: (SegTreeData a, Indexing i) => Tree a -> (i,i) -> Tree a
__applyPush node (l,r) = makeNode (fst' res) updatedLeft updatedRight
    where updatedLeft = makeNode ((snd' res) (value (left node)) (fst childSegments)) (left (left node)) (right (left node))
          updatedRight = makeNode ((trd' res) (value (right node)) (snd childSegments)) (left (right node)) (right (right node))
          res = push (value node) (l,r)
          childSegments = split (l,r)


__insert :: (SegTreeData a,Indexing i) => (a -> (i,i) -> a) -> (i,i) -> (i,i) -> Tree a -> Tree a
__insert f (a,b) (l,r) node
    | isEmpty intersection = node
    | isSingle (l,r) = makeLeaf $ f (value node) (l,r)
    | intersection == (l,r) = makeNode (f (value node) (l,r)) (left node) (right node)
    | otherwise = makeNode (merge (value newLeftNode) (fst childSegments) (value newRightNode) (snd childSegments) ) newLeftNode newRightNode
        where intersection = intersect (a,b) (l,r)
              pushedNode = __applyPush node (l,r)
              newLeftNode =  __insert f (a,b) (fst childSegments) (left pushedNode)
              newRightNode = __insert f (a,b) (snd childSegments) (right pushedNode)
              childSegments = split (l,r)


__query  :: (SegTreeData a, Indexing i) => (i,i) -> (i,i) -> Tree a -> a
__query (a,b) (l,r) node
    | isEmpty intersection  = emptyData
    | intersection == (l,r) = value node
    | otherwise = merge (__query (a, b) (fst childSegments) (left pushedNode)) (fst childSegments) (__query (a, b) (snd childSegments) (right pushedNode)) (snd childSegments)
    where intersection  = intersect (a,b) (l,r)
          pushedNode = __applyPush node (l,r)
          childSegments = split (l,r)



class (SegTreeData a, Indexing i) => SegTreeWrapper a b c i | a -> b c where
    inject :: b -> a -> (i,i) -> a
    extract :: a -> (i,i) -> c


data BaseSegTreeData b c = BaseSegTreeData { lazy::b, subtr::c} deriving(Eq)
instance (Show b, Show c) => Show (BaseSegTreeData b c) where
    show a = "(" ++ show (lazy a) ++ " " ++ show (subtr a) ++ ")"

class (Monoid b, Monoid c) => BaseOperationPair b c where
    putTag :: (Indexing i) => c -> (i,i) -> b -> c


instance (SymmetricMonoid m) => BaseOperationPair m m where
    putTag a (l,r) v = a `mappend` (Data.Semigroup.stimes (segmentLength (l,r)) v)

instance (Ord a,Bounded a,Num a) => BaseOperationPair (Sum a) (Maximum a) where
    putTag a (l,r) v = Maximum $ getSum $ Sum (getMaximum a) `mappend` v

instance (Num a) => BaseOperationPair (Assignment a) (Sum a) where
    putTag a (l,r) (Assignment Nothing) = a
    putTag a (l,r) (Assignment (Just v)) = Data.Semigroup.stimes (segmentLength (l,r)) (Sum v)

instance (BaseOperationPair b c) => SegTreeData (BaseSegTreeData b c) where
    emptyData = BaseSegTreeData mempty mempty
    merge x (lx,rx) y (ly,ry) = BaseSegTreeData mempty (subtr x `mappend` subtr y)
    
    push a (l,r) = (a', fl, fr)
        where a' = BaseSegTreeData mempty (subtr a)
              fl x (l',r') = BaseSegTreeData (lazy x `mappend` lazy a) (putTag (subtr x) (l',r') (lazy a) )
              fr x (l',r') = BaseSegTreeData (lazy x `mappend` lazy a) (putTag (subtr x) (l',r') (lazy a) )
    

instance (BaseOperationPair b c, Indexing i) => SegTreeWrapper (BaseSegTreeData b c) b c i where
    inject v a (l,r) = BaseSegTreeData (lazy a `mappend` v) (putTag (subtr a) (l,r) v)
    extract a (l,r) = subtr a



insert :: (SegTreeWrapper a b c i)  => b -> (i,i) -> IndexedTree a i -> IndexedTree a i
insert val (l, r) node = Root (__insert (inject val) (l,r) (border node) (rootOfTree node)) (border node)
    
query :: (SegTreeWrapper a b c i)  => (i,i) -> IndexedTree a i -> c    
query (a, b) node = extract (__query (a, b) (border node) (rootOfTree node)) (border node)
    
makeTree :: (SegTreeWrapper a b c i)  => (i, i) -> IndexedTree a i
makeTree (l,r)
    | isSingle (l,r)  = Root (makeLeaf $ emptyData) (l, r)
    | otherwise = Root node (l, r) 
    where childSegments = split (l,r)
          node = makeNode emptyData (rootOfTree $ makeTree (fst childSegments)) (rootOfTree $ makeTree (snd childSegments))