module SegArray where
    
import SegTreeBasis
import Monoids


buildSegmentList :: (Monoid a) => a -> (ClosedClosed, ClosedClosed) -> Int -> [a]
buildSegmentList v (a,b) 0 = []
buildSegmentList v (a,b) len
    | a <= 0 && b >= 0  =  v : buildSegmentList v (a-1, b-1) (len-1)
    | otherwise =  mempty : buildSegmentList v (a-1,b-1) (len-1)

slowSegmentClear :: (Monoid a) => (ClosedClosed) -> [a] -> [a]
slowSegmentClear a [] = []
slowSegmentClear a (x:xs)
    | a == 0 = (mempty : res)
    | otherwise = (x : res)
    where res = slowSegmentClear (a-1) xs

slowSegmentInsert :: (Monoid a) => a -> (ClosedClosed, ClosedClosed) -> [a] -> [a]
slowSegmentInsert v (a,b) list = zipWith mappend (buildSegmentList v (a,b) (length list)) list

slowSegmentQuery :: (Monoid a) => (ClosedClosed, ClosedClosed) -> [a] -> a
slowSegmentQuery (a,b) [] = mempty
slowSegmentQuery (a,b) (x:xs)
    | a <= 0 && b >= 0 = x `mappend` slowSegmentQuery (a-1,b-1) xs
    | otherwise = slowSegmentQuery (a-1,b-1) xs