{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE RankNTypes #-}

import SegTreeBasis
import PointSegTree
import Test.Hspec
import Test.QuickCheck
import Monoids
import SegArray

class PointSegStr s a i | s -> a i where
    psChange :: i -> a -> s -> s
    psQuery  :: (i,i) -> s -> a


instance (Monoid a, Indexing i) => PointSegStr (IndexedTree a i) a i where
    psChange a v tree = change a v tree
    psQuery (a,b) tree = query (a,b) tree

    
instance (Monoid a) => PointSegStr [a] a ClosedClosed where
    psChange a v s = slowSegmentInsert v (a,a) (slowSegmentClear a s)
    psQuery (a,b) s = slowSegmentQuery (a,b) s


data PointSegTest a i =
    PSQuery (i,i)
    | PSChange i a
    deriving (Show)
    

doTests :: (PointSegStr s a i) => [PointSegTest a i] -> s-> [a]
doTests [] t = []
doTests ((PSQuery (i,j)) : xs) t = (psQuery (i,j) t)  : doTests xs t
doTests ((PSChange i a) : xs) t = doTests xs (psChange i a t)


treeTestSize = 15 :: Integer

instance Arbitrary (PointSegTest (Sum Integer) ClosedClosed) where
    arbitrary = do
        t <- choose (0, 1) :: Gen Int
        case t of
            0 -> do
                v <- arbitrary :: Gen Integer
                pos <- choose (0,treeTestSize)
                return $ PSChange  (ClosedClosed pos) (Sum v) 
            1 -> do
                a <- choose (0,treeTestSize)
                b <- choose (a,treeTestSize)
                return $ PSQuery (ClosedClosed a, ClosedClosed b)


prop1 :: [PointSegTest (Sum Integer) ClosedClosed] -> Bool
prop1 list = (doTests list (makeTree (0,ClosedClosed treeTestSize) :: IndexedTree (Sum Integer) ClosedClosed))  == (doTests list (buildSegmentList 0 (0,0) (fromIntegral (treeTestSize+1)) :: [Sum Integer]) )



main = hspec $ do
  describe "Segment tree, point-segment version" $ do
    describe "Sum query" $ do
        it "returns 0 on newly built tree" $
            let tree :: IndexedTree (Sum Int) ClosedClosed
                tree = makeTree (0, 10::ClosedClosed)
            in query (3,7) tree `shouldBe` 0
        it "returns changed value after change on newly built tree" $
            let tree = change 5 (Sum 123) (makeTree (0, 10::ClosedClosed))
            in query (0,10) tree `shouldBe` 123 
        it "returns replaced value after change on tree with one node already changed" $
            let tree = change 2 (Sum 89) (makeTree (0, 10::ClosedClosed))
                tree' = change 2 (-76) tree
            in query (0,10) tree' `shouldBe` -76 
        it "works on single node tree" $
            let tree = change 0 (Sum 13) (makeTree (0,0::ClosedClosed))
                tree' = change 0 7 tree
                tree'' = change  0 (2+query (0,0) tree') tree'
            in query (0,0) tree'' `shouldBe` 9
        it "works on random quickCheck data" $
            property prop1

