{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FunctionalDependencies #-}


module PointSegTree where

import SegTreeBasis
import Monoids

makeTree :: (Monoid a, Indexing i) => (i, i) -> IndexedTree a i
makeTree (l,r)
    | isSingle (l,r)  = Root (makeLeaf mempty) (l, r)
    | otherwise = Root node (l, r) 
    where childSegments = split (l,r)
          node = makeNode mempty (rootOfTree $ makeTree (fst childSegments)) (rootOfTree $ makeTree (snd childSegments))

change' :: (Monoid a, Indexing i) => i -> a -> (i, i) -> Tree a -> Tree a
change' pos val (l, r) node
    | isSingle (l,r)     = makeLeaf val
    | inside leftChildSegment pos = 
        let newLeft  = change' pos val leftChildSegment (left node)
            conc = (value newLeft) `mappend` (value (right node))
        in  makeNode conc newLeft (right node)
    | inside rightChildSegment  pos = 
        let newRight = change' pos val rightChildSegment (right node)
            conc = value (left node) `mappend` value newRight
        in  makeNode conc (left node) newRight
    | otherwise = error "index not in left nor right subsegment"
    where  childSegments = split (l,r)
           leftChildSegment = fst childSegments
           rightChildSegment = snd childSegments

change :: (Monoid a, Indexing i) => i -> a -> IndexedTree a i -> IndexedTree a i
change pos val node = Root (change' pos val (border node) (rootOfTree node)) (border node)

query' :: (Monoid a,Indexing i) => (i, i) -> (i, i) -> Tree a -> a
query' (a, b) (l, r) node
    | isEmpty intersection  = mempty
    | intersection == (l,r) = value node
    | otherwise = (query' (a, b) (fst childSegments) (left node)) `mappend` (query' (a, b) (snd childSegments) (right node))
    where intersection  = intersect (a,b) (l,r)
          childSegments = split (l,r)

query :: (Monoid a, Indexing i) => (i, i) -> IndexedTree a i -> a
query (a, b) node = query' (a, b) (border node) (rootOfTree node)
