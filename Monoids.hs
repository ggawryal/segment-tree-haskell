{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Monoids where
    
import Data.Bits
import Data.Semigroup hiding (Product, Sum, getProduct, getSum)
import Data.Semigroup hiding (Product, Sum, getProduct, getSum)
import Data.Monoid hiding (Product, Sum, getProduct, getSum)
class (Monoid a) => SymmetricMonoid a


newtype Xor a = Xor {getXor :: a} deriving (Eq,Ord,Read)
newtype Sum a = Sum {getSum :: a} deriving (Eq,Ord,Read,Bounded,Num)
newtype Product a = Product {getProduct :: a} deriving (Eq,Ord,Read,Bounded,Num)
newtype Maximum a = Maximum {getMaximum :: a} deriving (Eq,Ord,Read,Bounded,Num)
newtype Minimum a = Minimum {getMinimum :: a} deriving (Eq,Ord,Read,Bounded,Num)
newtype Assignment a = Assignment {getAssignment :: Maybe a} deriving (Eq,Ord,Read)

instance Show a => Show (Xor a) where
    show a = show $ getXor a
instance Show a => Show (Sum a) where
    show a = show $ getSum a  
instance Show a => Show (Product a) where
    show a = show $ getProduct a
instance Show a => Show (Maximum a) where
    show a = show $ getMaximum a
instance Show a => Show (Minimum a) where
    show a = show $ getMinimum a  
instance Show a => Show (Assignment a) where
    show a = show $ getAssignment a  

instance Bits a => Semigroup (Xor a) where
    (<>) (Xor x) (Xor y) = Xor (x `xor` y)
instance Bits a => Monoid (Xor a) where  
    mempty = Xor zeroBits
    Xor x `mappend` (Xor y) = Xor (x `xor` y)
instance Bits a => SymmetricMonoid (Xor a)


instance Num a => Semigroup (Sum a) where
    (<>) (Sum x) (Sum y) = Sum (x + y)
instance Num a => Monoid (Sum a) where  
    mempty = 0
    Sum x `mappend` (Sum y) = Sum (x + y)
instance Num a => SymmetricMonoid (Sum a)


instance Num a => Semigroup (Product a) where
    (<>) (Product x) (Product y) = Product (x * y)
instance Num a => Monoid (Product a) where  
    mempty = 1
    Product x `mappend` (Product y) = Product (x * y)
instance Num a => SymmetricMonoid (Product a)


instance (Ord a) => Semigroup (Maximum a) where
    (<>) (Maximum x) (Maximum y) = Maximum (if x > y then x else y)
instance (Ord a,Bounded a) => Monoid (Maximum a) where  
    mempty = minBound
    Maximum x `mappend` (Maximum y) = Maximum (if x > y then x else y)
instance (Ord a,Bounded a) => SymmetricMonoid (Maximum a)


instance (Ord a) => Semigroup (Minimum a) where
    (<>) (Minimum x) (Minimum y) = Minimum (if x < y then x else y)
instance (Ord a,Bounded a) => Monoid (Minimum a) where  
    mempty = maxBound
    Minimum x `mappend` (Minimum y) = Minimum (if x < y then x else y)
instance (Ord a,Bounded a) => SymmetricMonoid (Minimum a)

instance Semigroup (Assignment a) where
    (<>) (Assignment x) (Assignment Nothing) = Assignment x
    (<>) (Assignment x) (Assignment y) = Assignment y
instance Monoid (Assignment a) where  
    mempty = Assignment Nothing
    Assignment x `mappend` (Assignment Nothing) = Assignment x
    Assignment x `mappend` (Assignment y) = Assignment y