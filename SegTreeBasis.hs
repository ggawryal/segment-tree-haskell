{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FunctionalDependencies #-}

module SegTreeBasis where

    
import qualified Data.Tree
import qualified Data.Tree.Pretty

class (Eq a) => Indexing a where
    inside :: (a,a) -> a -> Bool
    intersect :: (a,a) -> (a,a) -> (a,a)
    split :: (a,a) -> ((a,a),(a,a))
    segmentLength :: (a,a) -> Integer
    isSingle :: (a,a) -> Bool
    isEmpty :: (a,a) -> Bool
    isEmpty (x,y)  = segmentLength (x,y) <= 0
    isSingle (x,y) = segmentLength (x,y) == 1
        

newtype ClosedClosed = ClosedClosed { closedClosedToInt :: Integer } deriving (Show,Read,Eq,Ord,Enum,Num,Real,Integral)

instance Indexing ClosedClosed where
    inside (x,y) z = x <= z && y >= z
    intersect (x,y) (x2,y2) = (max x x2 , min y y2)
    split (x,y) =
        let mid = (x+y) `div` 2
        in ((x,mid),(mid+1,y))
    segmentLength  (x, y) = closedClosedToInt $ y - x+1

newtype ClosedOpened = ClosedOpened { closedOpenedToInt :: Integer } deriving (Show,Read,Eq,Ord,Enum,Num,Real,Integral)

instance Indexing ClosedOpened where
    inside (x,y) z = x <= z && y > z
    intersect (x,y) (x2,y2) = (max x x2 , min y y2)
    split (x,y) =
        let mid = if y-x <= 4 then (x+y) `div` 2 else x + (x+y)*3 `div` 4 
        in ((x,mid),(mid,y))
    segmentLength  (x, y) = closedOpenedToInt $ y - x

data Tree a = EmptyTree | 
                Node {value :: a,
                      left :: (Tree a),
                      right :: (Tree a) 
                }deriving (Read,Eq)

toDataTree :: (Show a) => Tree a -> Data.Tree.Tree String
toDataTree EmptyTree = Data.Tree.Node "" [] 
toDataTree (Node value EmptyTree EmptyTree) = Data.Tree.Node (show value) []
toDataTree (Node value l r) = Data.Tree.Node (show value) [toDataTree l, toDataTree r]

instance (Show a) => Show (Tree a) where
    show tree = Data.Tree.Pretty.drawVerticalTreeWith 2 $ toDataTree tree

makeLeaf :: a -> Tree a
makeLeaf a = Node {value = a, left = EmptyTree, right = EmptyTree}
                            
makeNode :: a -> Tree a -> Tree a -> Tree a
makeNode v a b = Node {value = v, left = a, right = b}

data IndexedTree a i= Root {rootOfTree :: Tree a,
                             border :: (i,i)
                       }deriving (Read,Eq)

instance (Show a, Show i) => Show (IndexedTree a i) where
    show tree = (show $ rootOfTree tree)  ++ (show $ border tree)