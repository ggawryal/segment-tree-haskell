{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE RankNTypes #-}

import SegTreeBasis
import Monoids
import SegSegTree
import SegArray
import Test.Hspec
import Test.QuickCheck

class SegSegStr s a i | s -> a i where
    psInsert :: a -> (i,i) -> s -> s
    psQuery  :: (i,i) -> s -> a


instance (SymmetricMonoid a, Indexing i)  => SegSegStr (IndexedTree (BaseSegTreeData a a) i) a i where
    psInsert v (a,b) tree = insert v (a,b) tree
    psQuery (a,b) tree = query (a,b) tree

    
instance (Monoid a) => SegSegStr [a] a ClosedClosed where
    psInsert v (a,b) s = slowSegmentInsert v (a,b) s
    psQuery (a,b) s = slowSegmentQuery (a,b) s


data SegSegTest a i =
    PSQuery (i,i)
    | PSInsert a (i,i)
    deriving (Show)
    

doTests :: (SegSegStr s a i) => [SegSegTest a i] -> s -> [a]
doTests [] t = []
doTests ((PSQuery (i,j)) : xs) t = (psQuery (i,j) t)  : doTests xs t
doTests ((PSInsert a (i,j) ) : xs) t = doTests xs (psInsert a (i,j) t)


treeTestSize = 10 :: Integer

instance Arbitrary (SegSegTest (Sum Integer) ClosedClosed) where
    arbitrary = do
        t <- choose (0, 1) :: Gen Int
        case t of
            0 -> do
                v <- arbitrary :: Gen Integer
                a <- choose (0,treeTestSize)
                b <- choose (a,treeTestSize)
                return $ PSInsert (Sum v) (ClosedClosed a, ClosedClosed b) 
            1 -> do
                a <- choose (0,treeTestSize)
                b <- choose (a,treeTestSize)
                return $ PSQuery (ClosedClosed a, ClosedClosed b)


prop1 :: [SegSegTest (Sum Integer) ClosedClosed] -> Bool
prop1 list = (doTests list (makeTree (0, ClosedClosed treeTestSize) :: IndexedTree (BaseSegTreeData (Sum Integer) (Sum Integer)) ClosedClosed))  == (doTests list (buildSegmentList 0 (0,0) (fromIntegral (treeTestSize+1)) :: [Sum Integer]) )

--sample (arbitrary::Gen [SegSegTest (Sum Integer) ClosedClosed])

main = hspec $ do
  describe "Segment tree, segment-segment version" $ do
    describe "Sum insert, Sum query" $ do
        it "returns 0 on newly built tree" $
            let tree :: IndexedTree (BaseSegTreeData (Sum Int) (Sum Int)) ClosedClosed
                tree = makeTree (0, 10::ClosedClosed)
            in query (3,7) tree `shouldBe` 0 
        it "works on small sample" $
            let t1 = makeTree (0,10) :: (IndexedTree (BaseSegTreeData (Sum Int) (Sum Int)) ClosedClosed) 
                t2 = insert 1 (2,3) t1
                t3 = insert 5 (1,6) t2
                t4 = insert 2 (5,7) t3
            in [query (4,6) t4, query (4,7) t4, query (1,2) t4] `shouldBe` [19,21,11]
        it "works on random quickCheck data" $
            property prop1
